package com.denisloshkarev.socialapp.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Post {

    private String id;
    private String uid;
    private String author;
    private String text;
    private String title;
    private List<String> likes;
    private String url;
    private String thumb;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public String getThumbText() {
        if (text.length() > 160) {
            return text.substring(0, 160).trim() + "...";
        } else return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getLikes() {
        return likes != null ? likes : new ArrayList<String>();
    }

    public void setLikes(List<String> likes) {
        this.likes = likes;
    }

    @Override
    public String toString() {
        return "Post{" +
                "author='" + author + '\'' +
                ", text='" + text + '\'' +
                ", title='" + title + '\'' +
                ", likes=" + likes +
                '}';
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> postMap = new HashMap<>();
        if (this.uid != null) postMap.put("uid", uid);
        if (this.author != null) postMap.put("author", author);
        if (this.title != null) postMap.put("title", title);
        if (this.text != null) postMap.put("text", text);
        if (this.url != null) postMap.put("url", url);
        if (this.likes != null) postMap.put("likes", likes);
        if (this.thumb != null) postMap.put("thumb", thumb);
        return postMap;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
