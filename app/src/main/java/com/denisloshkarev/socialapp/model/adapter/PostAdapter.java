package com.denisloshkarev.socialapp.model.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.denisloshkarev.socialapp.R;
import com.denisloshkarev.socialapp.databinding.RowPostBinding;
import com.denisloshkarev.socialapp.model.Post;
import com.denisloshkarev.socialapp.ui.PostDetailsActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Denis Loshkarev.
 */

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> {

    private List<Post> mPosts;
    private Activity mContext;

    public PostAdapter(List<Post> posts) {
        this.mPosts = posts;
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_post, parent, false);
        if (!(parent.getContext() instanceof Activity)) throw new RuntimeException("Invalid context");
        mContext = (Activity) parent.getContext();
        return new PostViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PostViewHolder holder, final int position) {
        holder.setPost(mPosts.get(position));
    }

    @Override
    public int getItemCount() {
        return mPosts == null ? 0 : mPosts.size();
    }

    private List<Post> getPosts() {
        return mPosts;
    }

    class PostViewHolder extends RecyclerView.ViewHolder {

        private static final String LOG_TAG = "PostViewHolder";
        private RowPostBinding binding;

        PostViewHolder(View itemView) {
            super(itemView);
            this.binding = DataBindingUtil.bind(itemView);
        }

        void setPost(final Post post) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user == null) {
                Log.e(LOG_TAG, "User is NULL");
                return;
            }
            String uid = user.getUid();

            binding.setPost(post);
            binding.star.setOnClickListener(new PostStarClickListener(getAdapterPosition()));

            List<String> likes = post.getLikes();
            if (likes.contains(uid)) {
                binding.star.setImageResource(android.R.drawable.btn_star_big_on);
            } else {
                binding.star.setImageResource(android.R.drawable.btn_star_big_off);
            }

            final Context context = PostAdapter.this.mContext;
            Picasso.with(context)
                    .load(Uri.parse(post.getThumb()))
                    .placeholder(android.R.drawable.ic_menu_camera)
                    .error(android.R.drawable.ic_menu_camera)
                    .resize(200, 200).centerInside()
                    .into(binding.postThumb);
            binding.postCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, PostDetailsActivity.class);
                    intent.putExtra(PostDetailsActivity.KEY_INTENT_POST_ID, post.getId());
                    context.startActivity(intent);
                }
            });
            binding.notifyChange();
        }
    }

    private class PostStarClickListener implements View.OnClickListener {

        private int mPosition;

        private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        PostStarClickListener(int position) {
            this.mPosition = position;
        }

        @Override
        public void onClick(View v) {
            final View star = v;
            star.setEnabled(false);
            final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                final List<Post> posts = PostAdapter.this.getPosts();
                final String postId = posts.get(mPosition).getId();
                final DatabaseReference postRef = mDatabase.child("posts").child(postId);

                postRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Post post = dataSnapshot.getValue(Post.class);
                        if (post != null) {
                            List<String> likes = post.getLikes();
                            if (likes.contains(user.getUid())) {
                                likes.remove(user.getUid());
                            } else {
                                likes.add(user.getUid());
                            }
                            post.setId(postId);
                            post.setLikes(likes);

                            posts.set(mPosition, post);

                            Map<String, Object> postMap = post.toMap();
                            Map<String, Object> childUpdates = new HashMap<>();
                            childUpdates.put("/posts/" + postId, postMap);

                            final Activity activity = PostAdapter.this.mContext;

                            mDatabase.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    star.setEnabled(true);
                                    PostAdapter.this.notifyItemChanged(mPosition);
                                }
                            }).addOnFailureListener(activity, new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(activity, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
    }
}
