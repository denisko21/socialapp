package com.denisloshkarev.socialapp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.crash.FirebaseCrash;

/**
 * Created by Denis Loshkarev.
 */

public abstract class AbstractActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(
                new Thread.UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                        FirebaseCrash.log(paramThread.getName() + ": " + paramThrowable.getLocalizedMessage());
                        FirebaseCrash.report(paramThrowable);
                        System.exit(2);
                    }
                });
    }
}
