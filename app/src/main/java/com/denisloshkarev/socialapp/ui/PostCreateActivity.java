package com.denisloshkarev.socialapp.ui;

import android.Manifest;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.denisloshkarev.socialapp.R;
import com.denisloshkarev.socialapp.databinding.ActivityPostCreateBinding;
import com.denisloshkarev.socialapp.model.Post;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class PostCreateActivity
        extends AppCompatActivity
        implements EasyPermissions.PermissionCallbacks {

    public static final int REQUEST_CODE_BROWSE_VIDEO = 101;
    public static final int REQUEST_CODE_BROWSE_IMAGE = 102;
    public static final String SAVE_KEY_REQUEST_CODE = "requestCode";
    private static final int REQUEST_CODE_BROWSE_FILE = 100;
    private static final int REQUEST_CODE_IMAGE_CAPTURE = 103;
    private static final int REQUEST_CODE_VIDEO_CAPTURE = 104;
    private static final int REQUEST_CODE_CHECK_PERMISSIONS = 105;
    private static final int REQUEST_CODE_APP_SETTINGS = 106;

    private static final String LOG_TAG = "PostCreateActivity";
    private static final int MIN_TITLE_LENGTH = 4;
    private static final String FIREBASE_IMG_FOLDER = "image";
    private static final String FIREBASE_VID_FOLDER = "video";

    private static final String KEY_TITLE = "postTitle";
    private static final String KEY_TEXT = "postText";
    private static final String KEY_ATTACH_URI = "fileUri";
    private static final String KEY_ATTACH_CURRENT_PATH = "fileCurrentPath";
    private static final String KEY_ATTACH_FOLDER = "fileFolder";

    private ActivityPostCreateBinding binding;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private StorageReference mUserRef;
    private Uri mUri;
    private String mFolder;
    private String mCurrentFilePath;

    private DialogInterface.OnClickListener onPermissionDialogCancel;
    private boolean isTest;

    {
        onPermissionDialogCancel = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        };
    }

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    protected void onCreate(Bundle saveBundle) {
        super.onCreate(saveBundle);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        FirebaseStorage storage = FirebaseStorage.getInstance();

        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            onBackPressed();
            return;
        }
        mUserRef = storage.getReference().child(user.getUid());

        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_create);

        setTitle("Create Post");

        ActionBar toolbar = getSupportActionBar();
        if (toolbar != null) {
            toolbar.setDisplayHomeAsUpEnabled(true);
            toolbar.setDisplayShowHomeEnabled(true);
        } else {
            Log.e(LOG_TAG, "Toolbar not founded");
        }

        isTest = getIntent().getBooleanExtra(MainActivity.KEY_IS_TEST, false);

        addActions();

        if (saveBundle != null) {
            if (saveBundle.containsKey(KEY_TITLE))
                binding.title.setText(saveBundle.getString(KEY_TITLE));
            if (saveBundle.containsKey(KEY_TEXT))
                binding.postText.setText(saveBundle.getString(KEY_TEXT));
            if (saveBundle.containsKey(KEY_ATTACH_URI))
                mUri = Uri.parse(saveBundle.getString(KEY_ATTACH_URI));
            if (saveBundle.containsKey(KEY_ATTACH_CURRENT_PATH))
                mCurrentFilePath = saveBundle.getString(KEY_ATTACH_CURRENT_PATH);
            if (saveBundle.containsKey(KEY_ATTACH_FOLDER))
                mFolder = saveBundle.getString(KEY_ATTACH_FOLDER);
            if (mCurrentFilePath != null && mFolder != null) {
                switch (mFolder) {
                    case FIREBASE_IMG_FOLDER:
                        setImgThumb();
                        break;
                    case FIREBASE_VID_FOLDER:
                        setVideoThumb();
                        break;
                }
                binding.btnCreate.setEnabled(true);
            }
        }
    }

    private void addActions() {
        binding.btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isValidForm()) return;
                binding.btnCreate.setEnabled(false);
                binding.title.setEnabled(false);
                binding.postText.setEnabled(false);
                uploadTo(mFolder, mUri);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_TITLE, binding.title.getText().toString());
        outState.putString(KEY_TEXT, binding.postText.getText().toString());
        if (mUri != null) outState.putString(KEY_ATTACH_URI, mUri.toString());
        outState.putString(KEY_ATTACH_CURRENT_PATH, mCurrentFilePath);
        outState.putString(KEY_ATTACH_FOLDER, mFolder);
    }

    private boolean isValidForm() {
        boolean isValid = true;
        if (binding.title.getText().length() < MIN_TITLE_LENGTH) {
            binding.title.setError("Title is less than " + MIN_TITLE_LENGTH + " symbols");
            isValid = false;
        } else binding.title.setError(null);
        if (binding.postText.getText().length() < 1) {
            binding.postText.setError("Text is less than " + MIN_TITLE_LENGTH + " symbols");
            isValid = false;
        } else binding.postText.setError(null);
        if (mFolder == null || mUri == null) {
            Toast.makeText(this, "You need to select file", Toast.LENGTH_SHORT).show();
            isValid = false;
        }
        return isValid;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (!isTest) {
            checkPermissions();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK == resultCode) {
            switch (requestCode) {
                case REQUEST_CODE_BROWSE_FILE:
                    Uri uri = data.getData();
                    Integer req = data.getIntExtra(SAVE_KEY_REQUEST_CODE, -1);
                    checkFolder(uri, req);
                    break;
                case REQUEST_CODE_IMAGE_CAPTURE:
                    mFolder = FIREBASE_IMG_FOLDER;
                    mUri = Uri.parse(mCurrentFilePath);
                    setImgThumb();
                    binding.btnCreate.setEnabled(true);
                    break;
                case REQUEST_CODE_VIDEO_CAPTURE:
                    mFolder = FIREBASE_VID_FOLDER;
                    mUri = Uri.parse(mCurrentFilePath);
                    setVideoThumb();
                    binding.btnCreate.setEnabled(true);
                    break;
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setVideoThumb() {
        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(mCurrentFilePath, MediaStore.Video.Thumbnails.MINI_KIND);
        binding.thumb.setImageBitmap(bitmap);
    }

    private void setImgThumb() {
        String path = mCurrentFilePath;
        // Get the dimensions of the View
        int targetW = 500;
        int targetH = 500;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        //noinspection deprecation
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
        binding.thumb.setImageBitmap(bitmap);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

            File photoFile = new File(dir, "SocialApp_" + System.currentTimeMillis() + ".jpeg");
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            mCurrentFilePath = "file:" + photoFile.getAbsolutePath();

            startActivityForResult(takePictureIntent, REQUEST_CODE_IMAGE_CAPTURE);
        }
    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);

            File photoFile = new File(dir, "SocialApp_" + System.currentTimeMillis() + ".mp4");
            takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
            mCurrentFilePath = "file:" + photoFile.getAbsolutePath();

            startActivityForResult(takeVideoIntent, REQUEST_CODE_VIDEO_CAPTURE);
        }
    }

    private void checkFolder(Uri uri, Integer reqCode) {
        mUri = uri;
        mCurrentFilePath = getPath(this, uri);
        switch (reqCode) {
            case REQUEST_CODE_BROWSE_VIDEO:
                setVideoThumb();
                mFolder = FIREBASE_VID_FOLDER;
                break;
            case REQUEST_CODE_BROWSE_IMAGE:
                setImgThumb();
                mFolder = FIREBASE_IMG_FOLDER;
                break;
        }
        binding.btnCreate.setEnabled(true);
        binding.notifyChange();
    }

    private void uploadTo(String folder, Uri uri) {
        String name = mCurrentFilePath.substring(mCurrentFilePath.lastIndexOf("/") + 1);
        StorageReference childRef = mUserRef.child(folder + "/" + name);
        checkFileAndUpload(childRef, uri);
    }

    private void chooseFile() {
        Intent intent = new Intent(this, ChooseDialogActivity.class);
        startActivityForResult(intent, REQUEST_CODE_BROWSE_FILE);
    }

    private void checkPermissions() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (!EasyPermissions.hasPermissions(this, perms)) {
            EasyPermissions.requestPermissions(this, "This application needs the permit to your documents",
                    REQUEST_CODE_CHECK_PERMISSIONS, perms);
        }
    }

    private void checkFileAndUpload(final StorageReference childRef, final Uri localUri) {

        childRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Log.d(LOG_TAG, "File exist: " + uri.toString());
                AlertDialog.Builder builder = new AlertDialog.Builder(PostCreateActivity.this, R.style.AppCompatAlertDialog);
                builder.setTitle("Info")
                        .setMessage("You already have file with same name")
                        .setCancelable(false)
                        .setPositiveButton(R.string.btn_replace, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d(LOG_TAG, "Dialog button Replace click");
                                deleteAndUpload(childRef, localUri);
                            }
                        })
                        .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d(LOG_TAG, "Dialog button Cancel click");
                                binding.btnCreate.setEnabled(true);
                                binding.title.setEnabled(true);
                                binding.postText.setEnabled(true);
                            }
                        });
                AppCompatDialog alert = builder.create();
                alert.show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(LOG_TAG, e.getLocalizedMessage());
                uploadFile(childRef, localUri);
            }
        });
    }

    private void deleteAndUpload(final StorageReference childRef, final Uri uri) {
        childRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                uploadFile(childRef, uri);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showToast(R.string.firebase_storage_error_delete_file);
            }
        });
    }

    private void uploadFile(StorageReference childRef, Uri uri) {
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.fileInfo.setText("");
        final String fileName = uri.getLastPathSegment();
        UploadTask uploadTask = childRef.putFile(uri);

        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                Double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                binding.progressBar.setProgress(progress.intValue());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                assert taskSnapshot.getMetadata() != null;
                assert taskSnapshot.getMetadata().getDownloadUrl() != null;
                FirebaseUser user = mAuth.getCurrentUser();
                assert user != null;

                String nickName;
                if (user.getDisplayName() == null) nickName = user.getEmail();
                else nickName = user.getDisplayName();

                Uri downloadUrl = taskSnapshot.getMetadata().getDownloadUrl();
                Log.d(LOG_TAG, "Download URI: " + downloadUrl.toString());

                final Post post = new Post();
                post.setTitle(binding.title.getText().toString());
                post.setText(binding.postText.getText().toString());
                post.setUid(user.getUid());
                post.setUrl(downloadUrl.toString());
                post.setAuthor(nickName);

                ImageView imageView = binding.thumb;
                imageView.setDrawingCacheEnabled(true);
                imageView.buildDrawingCache();
                Bitmap bitmap = imageView.getDrawingCache();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();

                StorageReference thumbRef = mUserRef.child("thumb/" + mFolder + "/" + fileName);


                uploadFileData(thumbRef, data, post);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(LOG_TAG, e.getLocalizedMessage());
            }
        });
    }

    private void uploadFileData(StorageReference thumbRef, byte[] data, final Post post) {
        UploadTask uploadTask = thumbRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(LOG_TAG, e.getLocalizedMessage());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                assert downloadUrl != null;
                post.setThumb(downloadUrl.toString());

                newPost(post);

                binding.progressBar.setVisibility(View.GONE);
                binding.progressBar.setProgress(0);

                showToast(R.string.firebase_storage_info_file_uploded);
                finish();
            }
        });
    }

    private void newPost(Post post) {
        String key = mDatabase.child("posts").push().getKey();

        Map<String, Object> postMap = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + key, postMap);

        mDatabase.updateChildren(childUpdates)
                .addOnSuccessListener(PostCreateActivity.this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(PostCreateActivity.this, R.string.post_saved, Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(PostCreateActivity.this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(PostCreateActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void showToast(int strId) {
        Toast.makeText(PostCreateActivity.this, getString(strId), Toast.LENGTH_LONG).show();
    }

    private void showAlertDialogPermission() {
        new AppSettingsDialog.Builder(this, "We really need that permission")
                .setTitle("Permissions")
                .setPositiveButton("OK")
                .setNegativeButton("Cancel", onPermissionDialogCancel)
                .setRequestCode(REQUEST_CODE_APP_SETTINGS)
                .build()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_post_create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_choose) {
            chooseFile();
            return true;
        }

        if (id == R.id.action_image_capture) {
            dispatchTakePictureIntent();
            return true;
        }

        if (id == R.id.action_video_capture) {
            dispatchTakeVideoIntent();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        showAlertDialogPermission();
    }
}
