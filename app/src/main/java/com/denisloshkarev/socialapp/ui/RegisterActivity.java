package com.denisloshkarev.socialapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.denisloshkarev.socialapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity {

    private static final String LOG_TAG = "RegisterActivity";
    private EditText mInputEmail, mInputPassword;
    private Button mBtnRegister;
    private ProgressBar mProgressBar;
    private FirebaseAuth mAuth;
    public static final int MIN_PASSWRD_LENGTH = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        findViews();

        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = mInputEmail.getText().toString();
                String password = mInputPassword.getText().toString();

                if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    mInputEmail.setError(getString(R.string.error_toast_enter_email));
                    return;
                }
                if (password.isEmpty() || password.length() < MIN_PASSWRD_LENGTH) {
                    mInputPassword.setError(getString(R.string.error_toast_short_password)
                        + RegisterActivity.MIN_PASSWRD_LENGTH
                        + getString(R.string.error_toast_short_password2));
                    return;
                }

                mProgressBar.setVisibility(View.VISIBLE);
                mBtnRegister.setEnabled(false);

                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                mProgressBar.setVisibility(View.GONE);
                                mBtnRegister.setEnabled(true);
                            }
                        })
                        .addOnSuccessListener(RegisterActivity.this, new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {
                                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .addOnFailureListener(RegisterActivity.this, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(LOG_TAG, e.getLocalizedMessage());
                                Toast.makeText(RegisterActivity.this,
                                        e.getLocalizedMessage(),
                                        Toast.LENGTH_LONG).show();
                            }
                        });

            }
        });
    }

    private void findViews() {
        mBtnRegister = (Button) findViewById(R.id.register_button);
        mInputEmail = (EditText) findViewById(R.id.email);
        mInputPassword = (EditText) findViewById(R.id.password);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mProgressBar.setVisibility(View.GONE);
//        mBtnRegister.setEnabled(true);
//        mBtnLogin.setEnabled(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}