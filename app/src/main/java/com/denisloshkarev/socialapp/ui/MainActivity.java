package com.denisloshkarev.socialapp.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.denisloshkarev.socialapp.R;
import com.denisloshkarev.socialapp.databinding.ActivityMainBinding;
import com.denisloshkarev.socialapp.model.Post;
import com.denisloshkarev.socialapp.model.adapter.PostAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_CREATE_NEW_POST = 0;
    private static final String LOG_TAG = "MainActivity";
    public static final int MAX_POSTS_COUNT = 20;
    public static final String KEY_IS_TEST = "TEST";
    private ActivityMainBinding binding;

    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private FirebaseUser mUser;
    private List<Post> mPosts;
    private PostAdapter mAdapter;

    {
        mPosts = new ArrayList<>();
    }

    private boolean isTest = false;
    private boolean isFloatingBtnTest = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        Toolbar toolbar = binding.toolbar;
        toolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };
        mUser = mAuth.getCurrentUser();
    }

    private void addActions() {
        binding.newPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.newPost.setEnabled(false);
                if (!isFloatingBtnTest){
                    Intent intent = new Intent(MainActivity.this, PostCreateActivity.class);
                    intent.putExtra(KEY_IS_TEST, isTest);
                    startActivityForResult(intent, REQUEST_CODE_CREATE_NEW_POST);
                }
            }
        });
    }

    private void getLastPosts() {
        mPosts = new ArrayList<>();
        mAdapter = new PostAdapter(mPosts);
        setUpRecyclerView();

        Query postsQuery = mDatabase.child("posts").limitToLast(MAX_POSTS_COUNT);
        postsQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Post post = dataSnapshot.getValue(Post.class);
                post.setId(dataSnapshot.getKey());
                mPosts.add(post);
                mAdapter.notifyItemInserted(mPosts.size() - 1);
                Log.d(LOG_TAG, "Added to map " + dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(LOG_TAG, "onCancelled");
            }
        });
    }

    private void setUpRecyclerView() {
        RecyclerView mRecycler = binding.postsList;
        mRecycler.setHasFixedSize(true);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setReverseLayout(true);
        manager.setStackFromEnd(true);
        mRecycler.setLayoutManager(manager);
        mRecycler.setAdapter(mAdapter);
    }

    private void changeUserNickName() {
        mUser.updateProfile(new UserProfileChangeRequest.Builder().setDisplayName("Admin").build())
                .addOnSuccessListener(MainActivity.this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(MainActivity.this, "Update ok", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(MainActivity.this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(MainActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void signOut() {
        mAuth.signOut();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_CREATE_NEW_POST:
                    getLastPosts();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.progressBar.setVisibility(View.GONE);
        binding.newPost.setEnabled(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
        addActions();
        getLastPosts();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_sing_out) {
            signOut();
            return true;
        }
        if (id == R.id.action_refresh) {
            getLastPosts();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setTest(boolean test) {
        isTest = test;
    }

    public void setFloatingBtnTest(boolean floatingBtnTest) {
        isFloatingBtnTest = floatingBtnTest;
    }
}