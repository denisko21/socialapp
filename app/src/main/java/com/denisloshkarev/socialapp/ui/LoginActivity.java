package com.denisloshkarev.socialapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.denisloshkarev.socialapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AbstractActivity {

    private static final String LOG_TAG = "LoginActivity";
    private EditText mInputEmail, mInputPassword;
    private FirebaseAuth mAuth;
    private ProgressBar mProgressBar;
    private Button mBtnRegister, mBtnLogin;
    private boolean isTest = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null && !isTest) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }

        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        findViews();

        mAuth = FirebaseAuth.getInstance();

        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mInputEmail.getText().toString();
                String password = mInputPassword.getText().toString();

                if (email.isEmpty()) {
                    mInputEmail.setError(getString(R.string.error_toast_enter_email));
                    return;
                }
                mInputEmail.setError(null);
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    mInputEmail.setError(getString(R.string.wrong_email_format));
                    return;
                }
                mInputEmail.setError(null);
                if (password.isEmpty() || password.length() < RegisterActivity.MIN_PASSWRD_LENGTH) {
                    mInputPassword.setError(getString(R.string.error_toast_short_password)
                            + RegisterActivity.MIN_PASSWRD_LENGTH
                            + getString(R.string.error_toast_short_password2));
                    return;
                }
                mInputPassword.setError(null);

                mBtnRegister.setEnabled(false);
                mBtnLogin.setEnabled(false);

                if (!isTest) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    mProgressBar.setVisibility(View.GONE);
                                    mBtnRegister.setEnabled(true);
                                    mBtnLogin.setEnabled(true);
                                }
                            })
                            .addOnSuccessListener(LoginActivity.this, new OnSuccessListener<AuthResult>() {
                                @Override
                                public void onSuccess(AuthResult authResult) {
                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            })
                            .addOnFailureListener(LoginActivity.this, new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d(LOG_TAG, e.getLocalizedMessage());
                                    Toast.makeText(LoginActivity.this,
                                            e.getLocalizedMessage(),
                                            Toast.LENGTH_LONG).show();
                                }
                            });
                }
            }
        });
    }

    private void findViews() {
        mInputEmail = (EditText) findViewById(R.id.email);
        mInputPassword = (EditText) findViewById(R.id.password);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mBtnRegister = (Button) findViewById(R.id.btn_signup);
        mBtnLogin = (Button) findViewById(R.id.btn_login);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void setTest(boolean test) {
        isTest = test;
    }
}
