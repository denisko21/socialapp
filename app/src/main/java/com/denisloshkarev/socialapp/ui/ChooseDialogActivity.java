package com.denisloshkarev.socialapp.ui;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.denisloshkarev.socialapp.R;

public class ChooseDialogActivity extends AppCompatActivity {

    private static final String LOG_TAG = "ChooseDialogActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_dialog);
        setTitle("Attach");

        Button mBtnImg = (Button) findViewById(R.id.choose_dialog_btn_image);
        Button mBtnVid = (Button) findViewById(R.id.choose_dialog_btn_video);

        mBtnImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnImgClick();
            }
        });
        mBtnVid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnVidClick();
            }
        });
    }

    private void btnVidClick() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        intent.setType("video/*");
        startActivityForResult(intent, PostCreateActivity.REQUEST_CODE_BROWSE_VIDEO);
    }

    private void btnImgClick() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, PostCreateActivity.REQUEST_CODE_BROWSE_IMAGE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK == resultCode) {
            Uri uri = data.getData();
            Log.d(LOG_TAG, "Select file: " + uri.toString());
            Intent intent = new Intent();
            intent.setData(uri);
            intent.putExtra(PostCreateActivity.SAVE_KEY_REQUEST_CODE, requestCode);
            setResult(RESULT_OK, intent);
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
