package com.denisloshkarev.socialapp.ui;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;

import com.denisloshkarev.socialapp.R;
import com.denisloshkarev.socialapp.databinding.ActivityPostDetailsBinding;
import com.denisloshkarev.socialapp.model.Post;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class PostDetailsActivity extends AppCompatActivity {

    public static final String KEY_INTENT_POST_ID = "postID";
    private static final String LOG_TAG = "PostDetailsActivity";
    private static final String STATE_POSITION = "videoPosition";
    private ActivityPostDetailsBinding binding;
    private String mPostId;
    private MediaController mMediaControls;
    private ProgressDialog mProgressDialog;

    private int mPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_details);

        Toolbar toolbar = binding.toolbar;
        toolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(toolbar);

        mPostId = getIntent().getStringExtra(KEY_INTENT_POST_ID);
        Log.d(LOG_TAG, mPostId);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(true);
        } else {
            Log.e(LOG_TAG, "Toolbar not founded");
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        DatabaseReference mPostRef = FirebaseDatabase.getInstance().getReference()
                .child("posts").child(mPostId);

        mPostRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final Post post = dataSnapshot.getValue(Post.class);
                binding.postTitle.setText(post.getTitle());
                binding.postText.setText(post.getText());
                binding.postAuthor.setText(post.getAuthor());

                binding.imageView.setVisibility(View.VISIBLE);
                if (post.getUrl().contains("%2Fvideo%2F")) {
                    Picasso.with(PostDetailsActivity.this)
                            .load(Uri.parse(post.getThumb()))
                            .placeholder(android.R.drawable.ic_menu_camera)
                            .error(android.R.drawable.ic_menu_camera)
                            .resize(800, 800).centerInside()
                            .into(binding.imageView);
                    binding.imageViewPlay.setVisibility(View.VISIBLE);
                    binding.imageViewPlay.setImageResource(R.drawable.ic_movie_creation_white_48dp);
                    binding.imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            runVideo(post.getUrl());
                        }
                    });
                } else {
                    Picasso.with(PostDetailsActivity.this)
                            .load(Uri.parse(post.getUrl()))
                            .placeholder(android.R.drawable.ic_menu_camera)
                            .error(android.R.drawable.ic_menu_camera)
                            .resize(800, 800).centerInside()
                            .into(binding.imageView);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void runVideo(final String url) {
        binding.imageView.setVisibility(View.GONE);
        binding.imageViewPlay.setVisibility(View.GONE);
        binding.videoView.setVisibility(View.VISIBLE);

        if (mMediaControls == null) {
            mMediaControls = new MediaController(PostDetailsActivity.this);
        }

        mProgressDialog = new ProgressDialog(PostDetailsActivity.this);
        mProgressDialog.setProgressStyle(R.style.ProgressDialogStyle);
        mProgressDialog.setTitle("Wait for video");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();

        try {
            binding.videoView.setMediaController(mMediaControls);
            binding.videoView.setVideoURI(Uri.parse(url));
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
            e.printStackTrace();
        }

        binding.videoView.requestFocus();
        binding.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                binding.videoView.setVisibility(View.VISIBLE);
                mProgressDialog.dismiss();
                binding.videoView.seekTo(mPosition);
                if (mPosition == 0) {
                    binding.videoView.start();
                } else {
                    binding.videoView.pause();
                }
            }
        });
        binding.videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                binding.videoView.setVisibility(View.GONE);
                binding.imageView.setVisibility(View.VISIBLE);
                binding.videoView.setMediaController(null);
                mMediaControls = null;
                mProgressDialog.dismiss();
                return true;
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_POSITION, binding.videoView.getCurrentPosition());
    }
}
