package com.denisloshkarev.socialapp;

import android.content.ComponentName;
import android.os.Environment;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;

import com.denisloshkarev.socialapp.model.Post;
import com.denisloshkarev.socialapp.ui.MainActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;

/**
 * Created by Stanislav Volnjanskij on 25.08.16.
 * Edited by Denis Loshkarev.
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    private static final String USER_LOGIN = "denis@mail.ru";
    private static final String USER_PASSWORD = "123456";
    private static final String SIZE_KEY = "size";

    private static UiDevice uiDevice;
    private static int counter = 1;
    private static long start;
    @Rule
    public IntentsTestRule<MainActivity> rule = new IntentsTestRule<>(MainActivity.class);

    @BeforeClass
    public static void init() {
        start = System.currentTimeMillis();
        uiDevice = UiDevice.getInstance(getInstrumentation());
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(USER_LOGIN, USER_PASSWORD);
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @AfterClass
    public static void afterClass() {
        FirebaseAuth.getInstance().signOut();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("---------------------------------------------");
        System.out.println("Tests are finished in: " + (System.currentTimeMillis() - start));
        System.out.println("---------------------------------------------");
    }

    public static void takeScreenshot(String name) {
        File pic = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!pic.exists()) {
            pic.mkdir();
        }

        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/Screenshots");

        if (!dir.exists()) {
            dir.mkdir();
        }
        File file = new File(dir.getAbsolutePath() + "/" + String.format("%02d", counter) + "_" + name + ".png");
        uiDevice.takeScreenshot(file);
        counter++;
    }

    @Before
    public void beforeTest() {
        rule.getActivity().setTest(true);
    }

    @Test
    public void floatButtonClickActionTest() {
        onView(withId(R.id.new_post)).check(matches(isEnabled()));
        onView(withId(R.id.new_post)).perform(click());
        intended(hasComponent(new ComponentName("com.denisloshkarev.socialapp", "com.denisloshkarev.socialapp.ui.PostCreateActivity")));
    }

    @Test
    public void floatingBtnDoubleClickTest() {
        rule.getActivity().setFloatingBtnTest(true);
        onView(withId(R.id.new_post)).check(matches(isEnabled()));
        onView(withId(R.id.new_post)).perform(click());
        onView(withId(R.id.new_post)).check(matches(not(isEnabled())));
    }

    @Test
    public void scrollToItemAndTest() {
        Random rand = new Random();
        List<Post> posts = getPosts();
        Integer counter = rand.nextInt(6) + 5;
        for (int i = 0; i < counter; i++) {
            int pos = rand.nextInt(posts.size());
            Post post = posts.get(pos);
            onView(withId(R.id.posts_list))
                    .perform(RecyclerViewActions.actionOnItemAtPosition(pos, click()));

            onView(withId(R.id.post_title)).check(matches(withText(post.getTitle())));
            onView(withId(R.id.post_text)).check(matches(withText(post.getText())));
            onView(withId(R.id.post_author)).check(matches(withText(post.getAuthor())));
            pressBack();
        }
    }

    @Test
    public void menuItemsTest() {
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText(R.string.menu_item_refresh)).check(matches(isDisplayed()));
        onView(withText(R.string.menu_item_sign_out)).check(matches(isDisplayed()));
    }

    @Test
    public void menuSignOutClick() {
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText(R.string.menu_item_sign_out)).perform(click());
        intended(hasComponent(new ComponentName("com.denisloshkarev.socialapp", "com.denisloshkarev.socialapp.ui.LoginActivity")));
        takeScreenshot("MenuSignOut.jpg");
        onView(withId(R.id.email)).perform(replaceText(USER_LOGIN));
        onView(withId(R.id.password)).perform(replaceText(USER_PASSWORD));
        onView(withId(R.id.btn_login)).perform(click());
    }

    private List<Post> getPosts() {
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference postsRef = dbRef.child("posts");
        final ArrayList<Post> posts = new ArrayList<>();
        final Map<String, Integer> postsCount = new ConcurrentHashMap<>();
        postsCount.put(SIZE_KEY, MainActivity.MAX_POSTS_COUNT);

        postsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Long maxCount = Math.min(dataSnapshot.getChildrenCount(), postsCount.get(SIZE_KEY));
                Integer count = 0;
                if (maxCount < Integer.MAX_VALUE) {
                    count = maxCount.intValue();
                }
                postsCount.put(SIZE_KEY, count);
                Query postsQuery = postsRef.limitToLast(count);
                postsQuery.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Post post = dataSnapshot.getValue(Post.class);
                        post.setId(dataSnapshot.getKey());
                        posts.add(post);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        while (posts.size() < postsCount.get(SIZE_KEY)) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return posts;
    }
}
