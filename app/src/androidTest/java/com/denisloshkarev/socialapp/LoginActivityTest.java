package com.denisloshkarev.socialapp;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import android.support.test.uiautomator.UiDevice;

import com.denisloshkarev.socialapp.ui.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.Collection;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.runner.lifecycle.Stage.RESUMED;
import static org.hamcrest.Matchers.not;

/**
 * Created by Stanislav Volnjanskij on 25.08.16.
 * Edited by Denis Loshkarev.
 */
@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {


    private static final String USER_LOGIN = "denis@mail.ru";
    private static final String USER_PASSWORD = "123456";
    private static UiDevice uiDevice;
    private static Context context;
    private static long start;
    @Rule
    public ActivityTestRule<LoginActivity> rule = new ActivityTestRule<>(LoginActivity.class);
    private int counter = 1;
    private Activity currentActivity;

    @BeforeClass
    public static void init() {
        start = System.currentTimeMillis();
        context = getInstrumentation().getTargetContext();
        uiDevice = UiDevice.getInstance(getInstrumentation());
        FirebaseAuth.getInstance().signOut();
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("---------------------------------------------");
        System.out.println("Tests are finished in: " + (System.currentTimeMillis() - start));
        System.out.println("---------------------------------------------");
    }

    @Before
    public void setUp() {
        onView(withId(R.id.email)).perform(clearText());
        onView(withId(R.id.password)).perform(clearText());
    }

    @Test
    public void doubleClickTest() {
        LoginActivity activity = rule.getActivity();
        activity.setTest(true);
        onView(withId(R.id.email)).perform(replaceText(USER_LOGIN));
        onView(withId(R.id.password)).perform(replaceText(USER_PASSWORD));
        onView(withId(R.id.btn_login)).check(matches(ViewMatchers.isEnabled()));
        onView(withId(R.id.btn_login)).check(matches(isCompletelyDisplayed()));
        ;
        onView(withId(R.id.btn_login)).perform(click());
        onView(withId(R.id.btn_login)).check(matches(not(ViewMatchers.isEnabled())));
    }

    @Test
    public void emailFieldVerifyCheck() {
        onView(withId(R.id.email)).perform(clearText());
        onView(withId(R.id.btn_login)).perform(click());
        onView(withId(R.id.email)).check(matches(hasErrorText(context.getString(R.string.error_toast_enter_email))));
    }

    @Test
    public void emailFormatCheck() {
        onView(withId(R.id.email)).perform(replaceText("xxxx"));
        onView(withId(R.id.btn_login)).perform(click());
        onView(withId(R.id.email)).check(matches(hasErrorText(context.getString(R.string.wrong_email_format))));
    }

    @Test
    public void loginTest() {
        takeScreenshot("start");
        onView(withId(R.id.email)).perform(replaceText(USER_LOGIN));
        onView(withId(R.id.password)).perform(replaceText(USER_PASSWORD));
        takeScreenshot("field filled");
        onView(withId(R.id.btn_login)).perform(click());
        takeScreenshot("login");
        FirebaseAuth.getInstance().signOut();
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void getActivityInstance() {

        getInstrumentation().runOnMainSync(new Runnable() {
            public void run() {
                Collection<Activity> resumedActivities = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(RESUMED);
                if (resumedActivities.iterator().hasNext()) {
                    currentActivity = resumedActivities.iterator().next();
                }
            }
        });
    }

    public void takeScreenshot(String name) {
        File pic = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!pic.exists()) {
            pic.mkdir();
        }

        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/Screenshots");

        if (!dir.exists()) {
            dir.mkdir();
        }
        File file = new File(dir.getAbsolutePath() + "/" + String.format("%02d", counter) + "_" + name + ".png");
        uiDevice.takeScreenshot(file);
        counter++;
    }
}
